package com.onteir.services;

import com.github.wnameless.json.flattener.FlattenMode;
import com.github.wnameless.json.flattener.JsonFlattener;
import com.github.wnameless.json.flattener.JsonifyLinkedHashMap;
import com.onteir.dao.DaoFactory;
import com.onteir.dao.SubmissionDao;
import com.onteir.model.Submission;
import com.onteir.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SubmissionServiceImp implements SubmissionService {

	@Autowired
	DaoFactory daoFactory;

	@Override
	public void saveSubmission(Submission submission) {
		daoFactory.getDao(SubmissionDao.class).saveSubmission(submission);
	}

	@Override
	public Submission getSubmissionByID(String id) {
		return daoFactory.getDao(SubmissionDao.class).findUserSubmissionById(id);
	}

    @Override
    public Submission getSubmissionByID(String submissionID, boolean createIfNotExist) {

	    if(createIfNotExist)
	        return Optional.ofNullable(daoFactory.getDao(SubmissionDao.class).findUserSubmissionById(submissionID)).orElseGet(Submission::new);
	    else
	        return getSubmissionByID(submissionID);
    }

    public void deleteSubmission(String submissionId){
		daoFactory.getDao(SubmissionDao.class).deleteSubmissionByID(submissionId);
	}

	@Override
	public List<Submission> findAllUserSubmission(User loginUser) {
		return daoFactory.getDao(SubmissionDao.class).findAllUserSubmission(loginUser);
	}

	@Override
	public Map<String, List<String>>  getCustomizedFields(String submissionID) {

		Submission submission = daoFactory.getDao(SubmissionDao.class).findAllCustomizedFields(submissionID);
		Map<String, Object> map1 = new JsonFlattener(submission.getData().toString()).withFlattenMode(FlattenMode.KEEP_ARRAYS).flattenAsMap();
		Map<String, Object> map2 = map1.entrySet().stream().filter(e-> !(e.getValue() instanceof JsonifyLinkedHashMap)).collect(Collectors.toMap(
				e -> e.getKey().replaceFirst("(.*Customized\\.[^\\.]+).*","$1"), c -> c.getValue(),(k,v)-> ""));
		Map<String, List<String>> map4 = map2.keySet().stream().collect(Collectors.groupingBy(o -> o.substring(0, o.indexOf("Customized")-1)));
		Map<String, List<String>> map5 = map4.entrySet().stream().collect(Collectors.toMap(k -> k.getKey(), v -> v.getValue().stream().map(e -> e.replaceFirst("(.*Customized\\.)([^\\.]+).*", "$2")).collect(Collectors.toList())));

		return map5;

	}


}
