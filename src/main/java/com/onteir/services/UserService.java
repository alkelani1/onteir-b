package com.onteir.services;

import com.onteir.dao.DaoFactory;
import com.onteir.dao.UserDAO;
import com.onteir.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserService implements UserDetailsService {

	@Autowired
	private DaoFactory daoFactory;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

		User user = daoFactory.getDao(UserDAO.class).findUserByUserName(userName);
		Set<GrantedAuthority> authorities =  user.getRoles().stream().map(r->new SimpleGrantedAuthority(r.toString())).collect(Collectors.toSet());
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);
	}

	public void saveUser(User user) {
		daoFactory.getDao(UserDAO.class).saveUser(user);
	}

	public void setUsePasswordEncrypted(User user, String userPassword){
		user.setPassword(bCryptPasswordEncoder.encode(userPassword));
	}



	public List<User> getAllNonAdminUsers(){
		return daoFactory.getDao(UserDAO.class).findAllNonAdminUsers();
	}


	public void deleteUser(String id){
		daoFactory.getDao(UserDAO.class).deleteUser(id);
	}


	public User getUserByName(String userName){
		return daoFactory.getDao(UserDAO.class).findUserByUserName(userName);
	}

}
