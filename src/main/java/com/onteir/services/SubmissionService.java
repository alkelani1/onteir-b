package com.onteir.services;

import com.onteir.model.Submission;
import com.onteir.model.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface SubmissionService {

	 void saveSubmission(Submission submission);

	 Submission getSubmissionByID(String submissionID);

     Submission getSubmissionByID(String submissionID, boolean createIfNotExist);

	 void deleteSubmission(String submissionId);

	List<Submission> findAllUserSubmission(User loginUser);

	Map<String, List<String>> getCustomizedFields(String submissionID);
}
