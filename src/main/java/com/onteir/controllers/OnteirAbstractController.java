package com.onteir.controllers;

import com.onteir.model.Roles;
import com.onteir.model.User;
import com.onteir.services.SubmissionService;
import com.onteir.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

abstract public class OnteirAbstractController {

    @Autowired
    protected SubmissionService submissionService;

    @Autowired
    protected UserService userService;

    protected User getLoginUser() {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return userService.getUserByName(auth.getName());
    }


    protected boolean checkAccess(User user, boolean checkAdminRole){

        User loginUser = getLoginUser();
        boolean hasAccess = false;
        if(checkAdminRole){
            hasAccess = isAdmin(loginUser);
        }
        return (hasAccess || loginUser.equals(user));
    }

    protected boolean isAdmin(User user){
        return user.getRoles().contains(Roles.ROLE_ADMIN);
    }
}
