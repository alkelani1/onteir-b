package com.onteir.controllers;


import com.github.wnameless.json.flattener.FlattenMode;
import com.github.wnameless.json.flattener.JsonFlattener;
import com.onteir.model.Submission;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Controller
public class PreviewSubmissionController extends OnteirAbstractController {

    @RequestMapping("/user/submission/csv/{submissionID}")
    public void downloadCSV(@PathVariable("submissionID") String submissionID, HttpServletResponse httpServletResponse) throws IOException {


        httpServletResponse.setHeader("Content-Disposition", "attachment; filename=\"onteir_submission.csv\"");
        httpServletResponse.setContentType("text/csv");
        Submission submission = submissionService.getSubmissionByID(submissionID);

        if (!checkAccess(submission.getCreator(),true)){
            return;
        }

        Map<String, Object> map = new JsonFlattener(submission.getData().toString()).withSeparator(',').withKeyTransformer(e->e.replace(",",";").replace("[","").replace("]","")).withFlattenMode(FlattenMode.NORMAL).flattenAsMap();

        Comparator<String> c = new Comparator<String>() {
            final private Pattern p = Pattern.compile("^\\d+");
            @Override
            public int compare(String object1, String object2) {
                Matcher m = p.matcher(object1);
                Integer number1 = null;
                if (!m.find()) {
                    return object1.compareTo(object2);
                }
                else {
                    Integer number2 = null;
                    number1 = Integer.parseInt(m.group());
                    m = p.matcher(object2);
                    if (!m.find()) {
                        return object1.compareTo(object2);
                    }
                    else {
                        number2 = Integer.parseInt(m.group());
                        int comparison = number1.compareTo(number2);
                        if (comparison != 0) {
                            return comparison;
                        }
                        else {
                            return object1.compareTo(object2);
                        }
                    }
                }
            }
        };

        Map<String, Object> treeMap = map.entrySet().stream()
                .collect(Collectors.toMap(x -> x.getKey(),
                        x -> x.getValue(),
                        (oldValue, newValue) -> newValue,
                        ()->new TreeMap(c)));

        PrintWriter printWriter = httpServletResponse.getWriter();
        treeMap.forEach((k,v)->printWriter.write(String.format("%s, %s %s",k,v,System.getProperty("line.separator"))));

        printWriter.close();

    }


    @RequestMapping(value = "/user/deleteSubmission/{id}")
    public String deleteSubmission(@PathVariable("id") String id) {
        Submission submission = submissionService.getSubmissionByID(id);
        if(checkAccess(submission.getCreator(),true)){
            submissionService.deleteSubmission(id);
        }
        return "redirect:/submissions";
    }



    @RequestMapping("/user/submission/{id}")
    public String getSubmissionById(@PathVariable("id") String id, Model model) {
        Submission submission = submissionService.getSubmissionByID(id);
        if(!checkAccess(submission.getCreator(),true)) return "submissionPreview";
        model.addAttribute("submission", submission.getData());
        model.addAttribute("id", submission.getId());
        model.addAttribute("isCreator",checkAccess(submission.getCreator(),false));
        return "submissionPreview";
    }


}
