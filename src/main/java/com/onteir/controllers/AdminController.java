package com.onteir.controllers;


import com.onteir.dto.SubmissionPreviewDTO;
import com.onteir.formBeans.AddUserFormBean;
import com.onteir.model.Roles;
import com.onteir.model.Submission;
import com.onteir.model.User;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@Controller
public class AdminController extends OnteirAbstractController{

    private static String SUB_SERVICE = "subService";

    @GetMapping("/admin")
    public String goToAdminPage(Model model){
        return "redirect:/admin/users";
    }



    @GetMapping("/admin/addNewUser")
    public String getNewUserPanel(Model model) {
        model.addAttribute("newUser", new AddUserFormBean());
        model.addAttribute(SUB_SERVICE,"addUser");
        return "admin";
    }


    @PostMapping("/admin/addNewUser")
    public String addNewUser(@ModelAttribute AddUserFormBean newUser, Model model) {

        User user = new User();
        user.setUsername(newUser.getName());
        Set<Roles> roles = new HashSet<>();
        roles.add(Roles.ROLE_USER);
        user.setRoles(roles);
        user.setSubmissions(new ArrayList<>());
        try {
            userService.setUsePasswordEncrypted(user, newUser.getPassword());
            userService.saveUser(user);
            model.addAttribute("userSaved",true);
        }
        catch (DataAccessException e){
            model.addAttribute("error", "Can't Add this user [" + newUser.getName() + "] try different name or try again later");
            model.addAttribute(SUB_SERVICE,"addUser");
            return "admin";
        }
        return "redirect:/admin/users";
    }

    @RequestMapping("/admin/users")
    public String getAllUser(Model model){
        List<User> allUsers = userService.getAllNonAdminUsers();
        model.addAttribute("users",allUsers);
        model.addAttribute(SUB_SERVICE,"users");
        return "admin";
    }

    @RequestMapping("admin/deleteUser/{id}")
    public String deleteUser(@PathVariable("id") String id, Model model){
        userService.deleteUser(id);
        List<User> allUsers = userService.getAllNonAdminUsers();
        model.addAttribute("users",allUsers);
        model.addAttribute(SUB_SERVICE,"deleteUser");
        return "redirect:/admin/users";
    }


    @RequestMapping("/submissions")
    public String getAllSubmission(Model model) {

        List<Submission> submissions = submissionService.findAllUserSubmission(getLoginUser());
        List<SubmissionPreviewDTO> submissionDTOs = submissions.stream().map(e -> {

            Map<String, Map> submissionMap = e.getData().toMap();
            Map<String, Map> projectInformationMap = (Map<String, Map>) submissionMap.get("1");
            String projectNumber = (String) ((Map) projectInformationMap.get("Project Information").get("1")).get("Project number");
            String projectName = (String) ((Map) projectInformationMap.get("Project Information").get("2")).get("Project name");
            String projectAddress = (String) ((Map) projectInformationMap.get("Project Information").get("3")).get("Project address");

            return new SubmissionPreviewDTO(projectNumber, projectName, projectAddress, e.getId());

        }).collect(Collectors.toList());

        model.addAttribute("submissions", submissionDTOs);
        return "submissions";
    }




}
