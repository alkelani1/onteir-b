package com.onteir.controllers;

import com.github.wnameless.json.flattener.FlattenMode;
import com.github.wnameless.json.flattener.JsonFlattener;
import com.mongodb.BasicDBObject;
import com.onteir.model.Submission;
import com.onteir.model.User;
import com.onteir.utils.InputNamesUtil;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Controller
public class OnteirFormController extends OnteirAbstractController{

    @Autowired
    HttpServletRequest request;

    @RequestMapping("/")
    public String home() {
        return "index";
    }

    @RequestMapping("/admin")
    public String admin(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        model.addAttribute("user", auth.getName());
        return "admin";
    }


    @RequestMapping(value = "/onteirForm")
    public String getOnteirForm(Model model, @RequestParam(value = "submissionID",required = false) String submissionID) {

        Map<String, List<String>>  customizedFields = new HashMap<>();
        if(!StringUtils.isEmpty(submissionID)){
             customizedFields = submissionService.getCustomizedFields(submissionID);
        }
             model.addAttribute("customizedFields",customizedFields);
        return "form";
    }


    @RequestMapping(value = "/submitForm", method = RequestMethod.POST, produces = "text/plain")
    public @ResponseBody
    String submitForm(@RequestBody String data) throws ParseException, IOException {

        Map<String, Object> result = getResultsMap(data);
        Submission submission = new Submission();
        submission.setData(new BasicDBObject(result));
        User creator = getLoginUser();
        addCreatorToSubmission(submission, creator);
        submissionService.saveSubmission(submission);
        userService.saveUser(creator);
        return submission.getId();
    }


    @RequestMapping(value = "/submitForm/{id}", method = RequestMethod.PUT, produces = "text/plain")
    public @ResponseBody
    String updateForm(@RequestBody String data, @PathVariable("id") String submissionID) throws IOException, ParseException {

        Submission submission = submissionService.getSubmissionByID(submissionID, true);
        Map<String, Object> result = getResultsMap(data);
        submission.setData(new BasicDBObject(result));

        User loginUser = getLoginUser();
        User creator = submission.getCreator();
        if (creator != null && loginUser.equals(creator)) {
            submissionService.saveSubmission(submission);
            return submission.getId();
        }
        submission.setCreator(loginUser);
        submissionService.saveSubmission(submission);
        loginUser.getSubmissions().add(submission);
        userService.saveUser(loginUser);

        return submission.getId();
    }



    private void addCreatorToSubmission(Submission submission, User creator) {

        submission.setCreator(creator);
        creator.getSubmissions().add(submission);
    }

    private Map<String, Object> getResultsMap(@RequestBody String data) throws IOException, ParseException {

        JSONParser parser = new JSONParser();
        ContainerFactory orderedKeyFactory = new ContainerFactory() {
            public List creatArrayContainer() {
                return new LinkedList();
            }

            public Map createObjectContainer() {
                return new LinkedHashMap();
            }

        };
        Map<String, Object> result = InputNamesUtil.decodeKeys((Map<String, Object>) parser.parse(data, orderedKeyFactory));
        return result;
    }




    @RequestMapping(value = "/onteirForm/{id}", produces = "application/json")
	public @ResponseBody Map<String, Object> loadSavedForm(@PathVariable("id") String submissionID){

        Submission submission = submissionService.getSubmissionByID(submissionID);
        if (!checkAccess(submission.getCreator(),false)) return new HashMap<>();
        Map<String, Object> map1 = new JsonFlattener(submission.getData().toString()).withFlattenMode(FlattenMode.KEEP_ARRAYS).withSeparator('*').flattenAsMap();
        Map<String, Object> map2 = map1.entrySet().stream().filter(e -> !StringUtils.isEmpty(e.getValue())).collect(Collectors.toMap((e -> InputNamesUtil.getEncodedName(e.getKey().split("\\*"))), t -> t.getValue()));
        Map<String, Object> map3 = map2.entrySet().stream().collect(Collectors.toMap(
                e -> {
                    if (e.getValue() instanceof ArrayList) {
                        return e.getKey() + "[]";
                    } else {
                        return e.getKey();
                    }
                }, c -> c.getValue()
        ));
        return map3;
    }


}