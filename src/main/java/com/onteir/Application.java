package com.onteir;

import com.onteir.dao.DaoFactory;
import com.onteir.dao.mongoDao.SubmissionDaoImp;
import com.onteir.model.Roles;
import com.onteir.model.Submission;
import com.onteir.model.User;
import com.onteir.services.SubmissionService;
import com.onteir.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
//		Submission submission = context.getBean("test", Test.class).getSubmission("5a8b234b99531278fcd4e716");
	}
}

@Component
class Test {

	@Autowired
	UserService userService;


	@Autowired
    SubmissionService submissionService;

	@Autowired
	SubmissionDaoImp submissionDao;


	public Submission getCustomized(String id){
		return submissionDao.findAllCustomizedFields(id);
	}

	public Submission getSubmission(String submissionID){
	   return submissionService.getSubmissionByID(submissionID, true);
    }

	public void saveAdmin() {
		User user = new User();
		userService.setUsePasswordEncrypted(user,"shadan$$edward");
		Set<Roles> roles = new HashSet<>();
		roles.add(Roles.ROLE_ADMIN);
		user.setRoles(roles);
		user.setUsername("shadan");
		userService.saveUser(user);
	}

	public void saveUser() {
		User user = new User();
		userService.setUsePasswordEncrypted(user,"password");
		Set<Roles> roles = new HashSet<>();
		roles.add(Roles.ROLE_USER);
		user.setRoles(roles);
		user.setUsername("user");
		userService.saveUser(user);
	}

	@Autowired
	DaoFactory dao;

}