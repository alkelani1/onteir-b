package com.onteir.dao;

import com.onteir.model.User;

import java.util.List;


public interface UserDAO {

	public void saveUser(User user);

	public User findUserByUserName(String userName);

	public User findUserByUserId(String id);

	public void updateUser(User user);

	public List<User> findAllNonAdminUsers();

	public void deleteUser(String id);

}
