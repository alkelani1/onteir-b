package com.onteir.dao;

import com.onteir.model.Submission;
import com.onteir.model.User;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import java.io.Serializable;
import java.util.List;

;

@NoRepositoryBean
public interface SubmissionDao extends Repository<SubmissionDao, Serializable> {

    Submission findUserSubmissionById(String id);

    List<Submission> findAllUserSubmission(User user);

    void saveSubmission(Submission submission);

    void deleteSubmissionByID(String submissionId);

    Submission findAllCustomizedFields(String submissionID);

}
