package com.onteir.dao.mongoDao;

import com.onteir.dao.UserDAO;
import com.onteir.model.Roles;
import com.onteir.model.User;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.regex.Pattern;

@Repository
public class UserDaoImp implements UserDAO{

	
	@Autowired
	private MongoOperations mongoOperations;
	
	@Override
	public void saveUser(User user) {
		mongoOperations.save(user);
	}

	@Override
	public User findUserByUserName(String userName) {
		
		Query q = new Query(Criteria.where("username").is(userName));
		return mongoOperations.findOne(q,User.class);
	}

	@Override
	public User findUserByUserId(String id) {
		id = Pattern.quote(id);
		Query q = new Query(Criteria.where("deleted").is(Boolean.FALSE).and("_id").is(new ObjectId(id)));
		return mongoOperations.findOne(q, User.class);
	}

	@Override
	public void updateUser(User user) {
//		mongoOperations.updateFirst(query, update, entityClass)
	}

	@Override
	public List<User> findAllNonAdminUsers() {
		Query q = new Query(Criteria.where("roles").is(Roles.ROLE_USER.toString()));
		return mongoOperations.find(q,User.class);
	}

	@Override
	public void deleteUser(String id) {
		Query query = new Query(Criteria.where("_id").is(new ObjectId(id)));
		mongoOperations.remove(query,User.class);
	}


}
