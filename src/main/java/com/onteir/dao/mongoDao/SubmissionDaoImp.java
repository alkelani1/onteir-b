package com.onteir.dao.mongoDao;

import com.onteir.dao.SubmissionDao;
import com.onteir.model.Roles;
import com.onteir.model.Submission;
import com.onteir.model.User;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SubmissionDaoImp implements SubmissionDao {

	@Autowired
	private MongoOperations mongoOperations;

	public Class<? extends Submission> getType() {
		return type;
	}

	private final Class<? extends Submission> type;

	public <T extends Submission> SubmissionDaoImp(Class<T> type) {
		this.type = type;
	}

	public SubmissionDaoImp() {
		this.type = Submission.class;
	}

	@Override
	public List<Submission> findAllUserSubmission(User user) {

		Query q = new Query();
		q.fields().include("_id");
		q.fields().include("createdDate");
		q.fields().include("data.1.Project Information");
		if (!user.getRoles().contains(Roles.ROLE_ADMIN))
			q.addCriteria(Criteria.where("creator.$id").is(new ObjectId(user.getId())));
		return mongoOperations.find(q,Submission.class);
	}

	public Submission findAllCustomizedFields(String submissionID){

		Query query = new Query();
		query.fields().include("data.1.Project Information.Customized");
		query.fields().include("data.10.Communication: Coordination and Clash Detection.Customized");
		query.fields().include("data.11.Asset Information Model Delivery Strategy.Customized");
		query.fields().include("data.14.Concept Design.a- Data Drops.Customized");
		query.fields().include("data.15.developed Design.a-1- Data Drops.Customized");
		query.fields().include("data.15.developed Design.a-2- CDM Data Drops.Customized");
		query.fields().include("data.16.Technical Design.a-1- Data Drops.Customized");
		query.fields().include("data.16.Technical Design.a-2- CDM Data Drops.Customized");
		query.fields().include("data.17.Construction.a- CDM Data Drops.Customized");
		query.fields().include("data.18.Handover and closeout.a- CDM Data Drops.Customized");
		query.fields().include("data.19.In-Use.a- CDM Data Drops.Customized");
		query.fields().include("data.2.Roles.Customized");
		query.fields().include("data.3.Responsibilities.Task 1: Common Data Environment.Customized");
		query.fields().include("data.3.Responsibilities.Task 2: Resources.Customized");
		query.fields().include("data.3.Responsibilities.Task 3: Project Strategy.Customized");
		query.fields().include("data.3.Responsibilities.Task 4: Geometry.Customized");
		query.fields().include("data.3.Responsibilities.Task 5: Data.Customized");
		query.fields().include("data.3.Responsibilities.Task 6: Construction Management.Customized");
		query.fields().include("data.3.Responsibilities.Task 7: Quality Assurance And Control.Customized");
		query.fields().include("data.3.Responsibilities.Task 8: Meetings.Customized");
		query.fields().include("data.3.Responsibilities.Task 9: Reporting And Governance.Customized");
		query.fields().include("data.4.Project team role.Customized");
		query.fields().include("data.5.Standard.Customized");
		query.fields().include("data.6.Ownership of the Model.Stage.Customized");
		query.fields().include("data.7.Data security.Home and Mobile Working.Customized");
		query.fields().include("data.7.Data security.Incident Management.Customized");
		query.fields().include("data.7.Data security.Information Risk Management Regime.Customized");
		query.fields().include("data.7.Data security.Malware Protection.Customized");
		query.fields().include("data.7.Data security.Managing User Previlages.Customized");
		query.fields().include("data.7.Data security.Network Security.Customized");
		query.fields().include("data.7.Data security.Secure Configuration.Customized");
		query.fields().include("data.7.Data security.User Education and Awareness.Customized");
		query.fields().include("data.8.Software Platform.Use.Customized");
		query.fields().include("data.9.Coordinates.Customized");
		query.addCriteria(Criteria.where("_id").is(new ObjectId(submissionID)));
		return mongoOperations.findOne(query,Submission.class);

	}

	@Override
	public void saveSubmission(Submission submission) {
		mongoOperations.save(submission);
	}

	@Override
	public void deleteSubmissionByID(String submissionId) {
		Query query = new Query(Criteria.where("_id").is(new ObjectId(submissionId)));
		mongoOperations.remove(query,Submission.class);

	}

	@Override
	public Submission findUserSubmissionById(String id) {
		Query q = new Query(Criteria.where("_id").is(new ObjectId(id)));
		return mongoOperations.findOne(q, Submission.class);
	}
}
