package com.onteir.dao;

import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.onteir.model.Submission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class DaoFactory {
	
	@Autowired
	private ApplicationContext applicationContext;

	
	public <T> T getDao(Class<T> daoInterface){
		return applicationContext.getBean(daoInterface);
	}


    public static void addSubmission(DaoFactory daoFactory) {
        Submission submission = new Submission();
        submission.setData((DBObject) JSON.parse("{'input2':'value1'}"));
        daoFactory.getDao(SubmissionDao.class).saveSubmission(submission);
    }
}
