package com.onteir.utils;

import java.util.Base64;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.PropertyNamingStrategyBase;

public class CustomNamingStrategy extends PropertyNamingStrategyBase {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6591094800480598626L;

	@Override 
	public String translate(String propertyName) {
	        return new String(Base64.getDecoder().decode(propertyName));
	    }

}
