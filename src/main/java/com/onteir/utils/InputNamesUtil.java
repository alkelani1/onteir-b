package com.onteir.utils;

import com.onteir.Application;
import com.onteir.dao.DaoFactory;
import com.onteir.services.SubmissionService;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class InputNamesUtil {

	@Autowired
	static SubmissionService submissionService;

	@Autowired
	static DaoFactory daoFactory;

	public static void main(String[] args) {
		Map<String, Object> map = new HashMap<>();

//		for (int i : new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
//				25, 26, 27, 28, 29, 30 }) {
//			Map<String, Object> nestedMap = new HashMap<>();
//			Map<String, Object> nestedNestedMap = new HashMap<>();
//			Map<String, Object> nestedNested2Map = new HashMap<>();
//			Map<String, Object> nestedNested3Map = new HashMap<>();
//			Map<String, Object> nestedNested4Map = new HashMap<>();
//			Map<String, Object> nestedNested5Map = new HashMap<>();
//
//			nestedNested5Map.put(Base64.getEncoder().withoutPadding().encodeToString(("sss" + 64 * i).getBytes()),
//					("sss" + 64 * i));
//			nestedNested4Map.put(Base64.getEncoder().withoutPadding().encodeToString(("sss" + 32 * i).getBytes()),
//					nestedNested5Map);
//			nestedNested3Map.put(Base64.getEncoder().withoutPadding().encodeToString(("sss" + 16 * i).getBytes()),
//					nestedNested4Map);
//			nestedNested2Map.put(Base64.getEncoder().withoutPadding().encodeToString(("sss" + 8 * i).getBytes()),
//					nestedNested3Map);
//			nestedNestedMap.put(Base64.getEncoder().withoutPadding().encodeToString(("sss" + 4 * i).getBytes()),
//					nestedNested2Map);
//			nestedMap.put(Base64.getEncoder().withoutPadding().encodeToString(("sss" + 2 * i).getBytes()),
//					nestedNestedMap);
//			map.put(Base64.getEncoder().withoutPadding().encodeToString(("sss" + i).getBytes()), nestedMap);
//		}
//
//		TreeMap<String, String> tmap = new TreeMap<>();
//		tmap.put("a", "1");
//		tmap.put("b", "1");
//		tmap.put("c", "1");
//		tmap.put("d", "1");
//		System.out.println(tmap);
//
//		System.out.println(map);
//		System.out.println(decodeKeys(map));


		InputNamesUtil cl = SpringApplication.run(Application.class).getBean(InputNamesUtil.class);







	}

	public static String getEncodedName(String... inputNames) {
		String firstElement = Base64.getEncoder().withoutPadding().encodeToString(inputNames[0].getBytes());
		String name = Arrays.asList(inputNames).stream().skip(1)
				.map(e -> '[' + Base64.getEncoder().withoutPadding().encodeToString(e.getBytes()) + "]")
				.collect(Collectors.joining());
		return firstElement + name;
	}

	public static String getDecodedName(String name) {

		String firstPart = new String(Base64.getDecoder().decode(name.substring(0, name.indexOf("["))));
		Pattern x = Pattern.compile("\\]\\[|\\[|\\]");
		String otherPart = x.splitAsStream(name).skip(1).map(e -> "[" + new String(Base64.getDecoder().decode(e)) + "]")
				.collect(Collectors.joining());
		return firstPart + otherPart;
	}

	public static <T> T decodeKeys(T map) {

		if (!(map instanceof Map)) {
			return map;
		}

		Map<String, Object> castedMap = new TreeMap<>((Map<String, Object>) map);
		;
		List<String> keys = new ArrayList<String>(castedMap.keySet());
		int n = keys.size();
		while (n-- > 0) {
			String key = keys.get(n);
			castedMap.put(validateKey(new String(Base64.getDecoder().decode(key))), decodeKeys(castedMap.get(key)));
			castedMap.remove(key);
		}
		return (T) (castedMap);
	}

	public static void flatten(){


	}

	private static String validateKey(String key){
		return StringEscapeUtils.escapeHtml4(key).replace(".",",");
	}

}