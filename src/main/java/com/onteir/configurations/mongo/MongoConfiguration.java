package com.onteir.configurations.mongo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

@Configuration
@EnableMongoAuditing
@PropertySource("classpath:${spring.profiles.active}/mongo.properties")
public class MongoConfiguration extends AbstractMongoConfiguration {
	
	@Autowired
	private Environment environment;
	
	@Override
	protected String getDatabaseName() {
		return environment.getProperty("mongo.dbname");
	}

	@Override
	public MongoClient mongo() throws Exception {
		
		List<ServerAddress> replicaSet = new ArrayList<>();
		for (String entry : environment.getProperty("mongo.replica-set").split(",")) {
			String[] pair = entry.split(":");
			replicaSet.add(new ServerAddress(pair[0].trim(), Integer.parseInt(pair[1].trim())));
		}
		
		List<MongoCredential> credentialsList = Arrays.asList(MongoCredential.createCredential(environment.getProperty("mongo.username"),
				getDatabaseName(), environment.getProperty("mongo.password").toCharArray()));
		
		return new MongoClient(replicaSet, credentialsList);
	}
}
	

