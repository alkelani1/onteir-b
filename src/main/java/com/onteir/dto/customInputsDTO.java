package com.onteir.dto;

public class customInputsDTO {


    String name;

    String encodedName;

    public customInputsDTO(String name, String encodedName) {
        this.name = name;
        this.encodedName = encodedName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEncodedName() {
        return encodedName;
    }

    public void setEncodedName(String encodedName) {
        this.encodedName = encodedName;
    }




}

