package com.onteir.dto;

public class SubmissionPreviewDTO {

    private String projectNumber;
    private String projectName;
    private String projectAddress;
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SubmissionPreviewDTO(String projectNumber, String projectName, String projectAddress, String id) {
        this.projectNumber = projectNumber;
        this.projectName = projectName;
        this.projectAddress = projectAddress;
        this.id = id;
    }

    public String getProjectNumber() {
        return projectNumber;
    }

    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectAddress() {
        return projectAddress;
    }

    public void setProjectAddress(String projectAddress) {
        this.projectAddress = projectAddress;
    }
}
