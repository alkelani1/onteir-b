












package com.onteir.model;

import com.mongodb.DBObject;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;


@Document(collection = Submission.COLLECTION)
public class Submission {

	public final static String COLLECTION = "submissions";
	
	@Id
	private String id;

	@CreatedDate
	private Date createdDate;

	private DBObject data;


	@DBRef(lazy = true)
	private User creator;


	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	public DBObject getData() {
		return data;
	}

	public void setData(DBObject data) {
		this.data = data;
	}

	public String getId() {
		return id;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

    public void setId(String id) {
        this.id = id;
    }

}
