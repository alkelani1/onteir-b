'use strict';

var keys = { 37: 1, 38: 1, 39: 1, 40: 1 };

function preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault) e.preventDefault();
    e.returnValue = false;
}

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

function clickStep(element) {

    var x = $(element).attr("href");
    $(x).click();
}

function clearCloned(step) {
    $('.steps.clearfix.cloned').find('li.current').removeClass('current').addClass('done');
    $('#wizard-t-' + step + "-cloned").parent('li').addClass("current").removeClass("done");
}

function cloneTabs() {
    var  tabs = $("ul[role=tablist]").find('li:nth-child(n+13)').clone().each(function () {
        var a = $(this).find('a');
        a.attr('id', a.attr('id')+'-cloned');
        a.addClass("tooltipstered");
        a.attr('href',a.attr('href').replace('h','t'))
        a.attr('onclick','clickStep($(this))');
    })

    var tabsDiv = '<div class="steps clearfix cloned">' + '<ul role="tablist"></ul>';
    $(tabsDiv).insertAfter(".content.clearfix");
    $('.steps.clearfix.cloned').find('ul').append(tabs);

    $('#wizard-t-12,#wizard-t-13,#wizard-t-14,#wizard-t-15,#wizard-t-16,#wizard-t-17,#wizard-t-18,#wizard-t-19,#wizard-t-20,#wizard-t-21').each(function () {
        $(this).hide();
    });
}

function indentLi(el, top) {
    $('#' + el).parent('li').css({ "position": "absolute", "top": top, "left": "562%", "z-index": 1 });
}

function encodeInputName() {

    base64.settings = {
        "char62": "+",
        "char63": "/",
        "pad": null,
        "ascii": true
    };
    var name = base64.encode(arguments[0]);
    for (var i = 1; i < arguments.length; i++) {
        name = name + "[" + base64.encode(arguments[i]) + "]";
    }
    return name;
}

function confirmClose() {
    swal({
        title: 'Confirmation',
        type: 'warning',
        html: 'Are you sure you want to submit the form?',
        showCancelButton: true,
        confirmButtonText: 'Yes'
    }).then(function () {
        $('#timerTotal').timer('stop');
        submitForm();
    });
}

function createConsentLetter() {
    swal({
        title: 'Consent letter',
        type: 'info',
        allowOutsideClick: false,
        customClass: 'consent-letter',
        width: '750px',
        confirmButtonText: 'Confirm',
        html: '<h2>Introduction<h2>' + '<p>Welcome to the evaluation of the OntEIR tool, to assist employers in completely and correctly define their employer information requirements. The aim of this tool is to assist clients of all types in specifying and defining EIR for their projects, which will have benefits in producing better quality construction projects in terms of being on time, within budget and being able to respond to the client requirements' + 'This form is part of the validation for the OntEIR tool, participants are asked to fill in the OntEIR form that will enable the researcher to get feedback for the development of the tool</p>' + '<h2>Confidentiality</h2>' + '<p>No personal information will be collected that would identify you, and all your data will be anonymous. All data will be stored in a password protected electronic format. To help protect your confidentiality, the surveys will not contain information that will enable to identify you. Non-identifiable results of this study will be used for scholarly purposes and may be shared with the research team.</p>' + '<h2>Participation</h2>' + '<p>Please note that your participation in this study is completely voluntary. You may choose not to participate. However, if you do choose to participate, you may withdraw at any time while completing the form. If you don’t want to answer any of the questions you don’t have to.' + 'By submitting this survey, you are agreeing to participate and cannot withdraw after this point. If you decide to withdraw at any point, you will not be penalised.</p>' + '<p>Questions about the research or your rights as participants' + 'If you have any questions or concerns, feel free to contact the owner of this study at:</p>' + '<a href="mailto:Shadan.dwairi@uwe.ac.uk">Shadan.dwairi@uwe.ac.uk</a>' + '<h2>Consent</h2>' + '<p>Please confirm that you understand and agree to the following:<p>' + '<ul>' + '<li>I am over the age of 18</li>' + '<li>I have read through the information above and received enough information about the research.</li>' + '<li>I understand that by consenting to taking part in this study, I can still withdraw at any time without being obliged to give reasons.</li>' + '<li>I understand by submitting this survey, I cannot withdraw my data anymore.</li>' + '<li>I understand that I will not be personally identified at any report, and my name will be replaced by a number so that all the data can remain confidential.</li>' + '<li>I understand that this information will be used only for the purpose set out in the information page, and my consent is conditional upon the university complying with the duties and obligation under the Data Protection Act</li>' + '</ul>' + '<p>By consenting to take part in this study you are acknowledging that you understand that you are confirming to the agreement above. You agree to take part in this study</p>'

    });
}

function createToolTips() {
    $('#wizard-t-0').tooltipster({
        maxWidth: 300,
        trigger: 'hover',
        theme: 'tooltipster-light',
        content: 'This section contains the general requirements of the EIR, which are those requirements that are' + 'general to the whole project and not specified to a certain stage.'
    });

    $('#wizard-t-1').tooltipster({
        maxWidth: 300,
        trigger: 'hover',
        theme: 'tooltipster-light',
        content: 'Provide the general information for the project.'
    });

    $('#wizard-t-2').tooltipster({
        maxWidth: 300,
        trigger: 'hover',
        theme: 'tooltipster-light',
        content: 'Please define the roles and the names associated.'
    });

    $('#wizard-t-3').tooltipster({
        maxWidth: 300,
        trigger: 'hover',
        theme: 'tooltipster-light',
        content: 'BIM roles and responsibilities are described in this section. Please refer to the current version of the project execution plan for overall scopes of services.'
    });

    $('#wizard-t-4').tooltipster({
        maxWidth: 300,
        trigger: 'hover',
        theme: 'tooltipster-light',
        content: 'Please define the project team roles and names associated.'
    });

    $('#wizard-t-5').tooltipster({
        maxWidth: 300,
        trigger: 'hover',
        theme: 'tooltipster-light',
        content: 'Choose the relevant standards and their uses in the project.'
    });


    $('#wizard-t-6').tooltipster({
        maxWidth: 300,
        trigger: 'hover',
        theme: 'tooltipster-light',
        content: 'Define the ownership and license of the model at different stages.'
    });

    $('#wizard-t-7').tooltipster({
        maxWidth: 300,
        trigger: 'hover',
        theme: 'tooltipster-light',
        content: 'The following steps to cyber security should be facilitated by the organisation responding to the EIR here in.'
    });

    $('#wizard-t-8').tooltipster({
        maxWidth: 300,
        trigger: 'hover',
        theme: 'tooltipster-light',
        content: 'Define the different software platforms and their versions.'
    });


    $('#wizard-t-9').tooltipster({
        maxWidth: 300,
        trigger: 'hover',
        theme: 'tooltipster-light',
        content: 'Clarify the relationship between the origin and orientation of the Ordnance Survey Grid and Local Project Grid.'
    });


    $('#wizard-t-12-cloned').tooltipster({
        maxWidth: 300,
        trigger: 'hover',
        theme: 'tooltipster-light',
        content: 'This section will define the EIR for the stages in what is identified as the stage requirements.'
    });

    $('#wizard-t-13-cloned').tooltipster({
        maxWidth: 300,
        trigger: 'hover',
        theme: 'tooltipster-light',
        content: 'Define the dates in which each stage will start and finish.'
    });

    $('#wizard-t-15-cloned').tooltipster({
        maxWidth: 300,
        trigger: 'hover',
        theme: 'tooltipster-light',
        content: 'covers establishing the detailed brief, scope, scale, form and budget for the Project, including obtaining site studies and construction and specialist advice, and determining the initial design criteria, design options, cost estimates and selection of the preferred design option; and concludes with the Client approving the Concept Report, setting out the integrated concept for the Project and forming the basis for development of the design.'
    });

    $('#wizard-t-16-cloned').tooltipster({
        maxWidth: 300,
        trigger: 'hover',
        theme: 'tooltipster-light',
        content: 'Initial design indicative of area, height, volume. Area and Room Schedules Generic placeholders, elements are modelled as generalized systems or assemblies with approximate quantities, size, shape, location and orientation. Initial structural, MEP model coordination.'

    });

    $('#wizard-t-17-cloned').tooltipster({
        maxWidth: 300,
        trigger: 'hover',
        theme: 'tooltipster-light',
        content: 'Spatial design, building system and attributes that include: architectural, structural, MEP information. Specific assemblies, elements are modelled as specific assemblies accurate in terms of quantity, size, shape, location and orientation. Non-geometric information may also be attached to the model elements.'
    });

    $('#wizard-t-18-cloned').tooltipster({
        maxWidth: 300,
        trigger: 'hover',
        theme: 'tooltipster-light',
        content: 'Model elements are modelled as specific assemblies that are accurate in terms of size, shape, location, quantity and orientation with complete fabrication, assembly and detailing information. Non-geometric information may also be attached to model elements.'
    });

    $('#wizard-t-19-cloned').tooltipster({
        maxWidth: 300,
        trigger: 'hover',
        theme: 'tooltipster-light',
        content: 'Model and data Updated as required.'
    });

    //	$('#wizard-t-17').tooltipster({
    //		trigger: 'hover',
    //	    theme: 'tooltipster-light',
    //		content: ``
    //	});
}

function colorBackgroundDiv(tab) {
    if (tab < 12) {
        $('.steps.clearfix').not('.cloned').addClass('current-section');
        $('.steps.clearfix.cloned').removeClass('current-section');
    } else {
        $('.steps.clearfix.cloned').addClass('current-section');
        $('.steps.clearfix').not('.cloned').removeClass('current-section');
    }
}

function submitForm() {

    var userJson = JSON.stringify($('form').serializeJSON({parseBooleans:true}));
    var url = window.location.pathname;
    var submissionID = getParameterByName("submissionID");
    if(submissionID){
        updateFrom(userJson, "/" + submissionID);
    } else {
        createNewForm(userJson)
    }
}

function createNewForm(userJson) {

    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $.ajax({
        type: "POST",
        url: "/submitForm",
        data: userJson,
        contentType: "application/json; charset=utf-8",
        beforeSend: function beforeSend(xhr) {
            xhr.setRequestHeader(header, token);
        },
        dataType: "text",
        success: function success(data) {
            location.href = "/user/submission/" + data;
        },
        failure: function failure(errMsg) {
            alert(errMsg); // handle it in a proper way
        }
    });
}

function updateFrom(userJson, submissionID) {

    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $.ajax({
        type: "PUT",
        url: "/submitForm" + submissionID,
        data: userJson,
        contentType: "application/json; charset=utf-8",
        beforeSend: function beforeSend(xhr) {
            xhr.setRequestHeader(header, token);
        },
        dataType: "text",
        success: function success(data) {
            location.href = "/user/submission/" + data;
        },
        failure: function failure(errMsg) {
            alert(errMsg); // handle it in a proper way
        }
    });
}


function onStepsChanged(event, currentIndex, priorIndex) {
    alert(currentIndex);
    return true;
};

function disableScroll() {
    if (window.addEventListener) // older FF
        window.addEventListener('DOMMouseScroll', preventDefault, false);
    window.onwheel = preventDefault; // modern standard
    window.onmousewheel = document.onmousewheel = preventDefault; // older
    // browsers, IE
    window.ontouchmove = preventDefault; // mobile
    document.onkeydown = preventDefaultForScrollKeys;
}

function enableScroll() {
    if (window.removeEventListener) window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    window.ontouchmove = null;
    document.onkeydown = null;
}

function enableCurrentRow(element) {
    $(element).parents('.divTableRow').find('select').prop("disabled", !$(element).is(':checked')).chosen({display_selected_options : false, width : '100%'}).trigger('chosen:updated');
}

function toggleNextInputs(element, list) {
    $(element).parents('.divTableRow').find('input[type=text]').prop("disabled", !$(element).is(':checked'));
    var text = $(element).parents('.divTableCell').text().trim();
    var lists = $('.' + list);
    if ($(element).is(':checked')) {
        if (lists.find("option[value='" + text + "']").length == 0) {
            lists.append("<option value='" + text + "'>" + text + "</option>");
        }
    } else {
        lists.find("option[value='" + text + "']").remove();
    }
}

function overrideTab() {
    $('#function').keydown(function (e) {
        var keyCode = e.keyCode;
        if (keyCode = 9) {
            e.preventDefault();
            $('#addNew').select();
            $.fn.fullpage.moveSectionDown();
        }
    });
}

function initRoleList() {

    $('#roles-table').find('.divTableBody .divTableCell label').each(function () {
        $('.roles-list').append("<option value='" + $(this).text().trim() + "'>" + $(this).text().trim() + "</option>");
    });

    $('#team-roles-table').find('.divTableBody .divTableCell label').each(function () {
        $('.team-roles').append("<option value='" + $(this).text().trim() + "'>" + $(this).text().trim() + "</option>");
    });

    $('#get-checked-data').on('click', function (event) {
        event.preventDefault();
        var checkedItems = {},
            counter = 0;
        $("#check-list-box li.active").each(function (idx, li) {
            checkedItems[counter] = $(li).text();
            counter++;
        });
    });

    $('#add-new-role').on("click", function (e) {
        e.preventDefault();
        var name = encodeInputName('2', 'Roles', 'Customized', $('#roles-table').find('#new-role').val(), 'name');
        var email = encodeInputName('2', 'Roles', 'Customized', $('#roles-table').find('#new-role').val(), 'Email');
        var checkBoxName = encodeInputName('2', 'Roles', 'Customized', $('#roles-table').find('#new-role').val(), 'include role');
        addNewRecordToList('#roles-table', '.roles-list', name, email, checkBoxName);
    });

    $('#add-new-team-role').on('click', function (e) {
        e.preventDefault();
        var name = encodeInputName('4', 'Project team role','Customized', $('#team-roles-table').find('#new-role').val(), 'name');
        var checkBoxName = encodeInputName('4', 'Project team role','Customized', $('#team-roles-table').find('#new-role').val(), 'include role');
        addNewTeamRole('#team-roles-table', '.team-roles', name,  checkBoxName);
    });
}

function addNewStandard(e) {
    e.preventDefault();
    var text = $('#new-standard').val();
    var standardName = encodeInputName('5', 'Standard',  'Customized', text, 'is checked');
    var useFor = encodeInputName('5', 'Standard', 'Customized',text, 'Use for');
    if (text.trim()) {
        var newDiv = '<div class="divTableRow">' + '<div class="divTableCell">' + '<label><input ' + 'name="' + standardName + '" ' + 'type="checkbox" checked="checked" data-unchecked-value="false" value="true" checked="checked" ' + 'onchange="enableCurrentRow(this)"/>' + $('#new-standard').val() + '</label>' + '</div>' + '<div class="divTableCell">' + '<textarea rows="2" name=' + useFor + '>'+'</textarea>' + '</div>' + '</div>';

        $('#standard-table').find('.divTableBody').append(newDiv);
        $('.standard-list').chosen({
            display_selected_options: false,
            width: '100%' });
        $('#new-standard').val('');
    }
}

function addNewRecordToList(table, list, inputName1, inputName2, checkBoxName) {
    var encodedRoleCheckboxName = void 0,
        encodedRoleName = void 0;
    var text = $(table).find('#new-role').val();
    if (text.trim()) {
        var newDiv = '<div class="divTableRow">' + '<div class="divTableCell">' + '<label>' + '<input type="checkbox" checked="checked" data-unchecked-value="false" value="true" checked="checked" ' + 'name="' + checkBoxName + '" onchange="toggleNextInputs($(this))">' + text + '</label>' + '</div>' + '<div class="divTableCell">' + '<input type="text" name="' + inputName1 + '" >' + '</div>' + '<div class="divTableCell">' + '<input type="text" name="' + inputName2 + '" >' + '</div>' + '</div>';

        $(table).find('.divTableBody').append(newDiv);
        $(table).find('#new-role').val('');
        var lists = $(list);
        if (lists.find("option[value='" + text + "']").length == 0) {
            lists.append("<option value='" + text + "'>" + text + "</option>");
        }
    }
}

function addNewTeamRole(table, list, inputName1, checkBoxName) {
    var encodedRoleCheckboxName = void 0,
        encodedRoleName = void 0;
    var text = $(table).find('#new-role').val();
    if (text.trim()) {
        var newDiv = '<div class="divTableRow">' + '<div class="divTableCell">' + '<label>' + '<input type="checkbox" checked="checked" data-unchecked-value="false" value="true" checked="checked" ' + 'name="' + checkBoxName + '" onchange="toggleNextInputs($(this))">' + text + '</label>' + '</div>' + '<div class="divTableCell">' + '<input type="text" name="' + inputName1 + '" >' + '</div>'  + '</div>';

        $(table).find('.divTableBody').append(newDiv);
        $(table).find('#new-role').val('');
        var lists = $(list);
        if (lists.find("option[value='" + text + "']").length == 0) {
            lists.append("<option value='" + text + "'>" + text + "</option>");
        }
    }
}

function loadSavedData(submissionID){
    console.log(submissionID)
    if(!submissionID) return;

    $.ajax({
        url: "/onteirForm/" + submissionID
    }).then(function(data) {
        jQuery.each(data, function (name,value) {
            if(name.endsWith('[]')){
                $('select[name="' + name + '"]').val(value).trigger('chosen:updated');
                return;
            }

            var element = $('input[name="' + name + '"]');
            var type = $(element).attr("type");

            switch (type){
                case "text":
                    element.val(value);
                    return;
                case "checkbox":
                    var isChecked = element.is(":checked");
                    if((value == true && !isChecked) || (value == false && isChecked)) {
                        element.trigger("click");
                    }


                    return;
                case "radio":
                    if(value == "Use definitions provided by OntEIR")
                        $("#default").trigger("click");
                    else
                        $("#custom").trigger("click")
                    return;
            }

            $('textarea[name="' + name + '"]').val(value);

        })
    });
}


function getParameterByName(name, url) {
    
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


function enableLevelToEdit(){

    $('#level-table').find('textarea').each(function () {
        $(this).removeAttr("readonly");
    });
}


function disableLevelToEdit(){
    $('#level-table').find('textarea').each(function () {
        $(this).attr("readonly","readonly");
    });



}

function autoFill() {

    $("input[type='text']").each(function () {
        $(this).val("test");
    })


    $("select").each(function(){
        $(this).val($(this).find('option:eq(1)').text()).trigger("chosen:updated")});

    $('select[multiple="multiple"]').each(function () {
        $(this).val([$(this).find("option:eq(1)").text(),$(this).find("option:eq(2)").text()]).trigger("chosen:updated");
    })

}

function  addCustomInfo(event) {
    
    event.preventDefault();
    var inputName = $('#new-info').val();
    if(!inputName.trim()) return;
    var encodedName = encodeInputName('1','Project Information',"Customized",inputName);
    var div = "<div class='form-group'>"
    var label = "<label for='" + encodedName + "'>" + inputName +"</label>";
    var newInput = "<input class='form-control' type='text' name='" + encodedName + "' placeholder='"+inputName+"'/>";
    var divEnd = "</div>"
    $('#prjct-Info-div').append(div + label + newInput + divEnd);
    $('#new-info').val('');

}


function addCustomNewRecord(event, id, namePrefix){

    event.preventDefault();
    var customRecord = $('#' + id).next().find('#new-record').val();
    if(!customRecord.trim()) return;
    var inputName = namePrefix + encodeInputName('',customRecord);
    var newDiv = "<div class='divTableRow'> <div class='divTableCell'> <label><input data-unchecked-value='false' value='true' name='" + inputName + "' type='checkbox'>" + customRecord + "</label></div></div>";
    $('#' + id).find('.divTableBody').append(newDiv);
    $('#' + id).next().find('#new-record').val('');


}


function addCustomResponsibility(event, id, task) {

    event.preventDefault();
    var customRecord = $('#' + id).next().find('.new-record').val();
    if(!customRecord.trim()) return;
    var checkboxName = encodeInputName('3', 'Responsibilities', task, 'Customized', customRecord, 'is checked');
    var select1Name = encodeInputName('3', 'Responsibilities', task, 'Customized', customRecord, 'Responsibility Of') + '[]';
    var select2Name = encodeInputName('3', 'Responsibilities', task, 'Customized', customRecord, 'Consulted By') + '[]';
    var select3Name = encodeInputName('3', 'Responsibilities', task, 'Customized', customRecord, 'Informed By') + '[]';
    var select4Name = encodeInputName('3', 'Responsibilities', task, 'Customized', customRecord, 'Approved By') + '[]';
    var randomId = Math.random().toString(36).substring(7);
    var newDiv = "<div id='" + randomId + "'class='divTableRow'><div class='divTableCell'><label><input data-unchecked-value='false' value='true' onchange='enableCurrentRow(this)' name='" + checkboxName + "'type='checkbox'>" + customRecord + "</label></div>";
    var select1Div = "<div class='divTableCell'><select name='" + select1Name + "' class='form-control roles-list' disabled='disabled' multiple='multiple'></select></div>"
    var select21Div = "<div class='divTableCell'><select name='" + select2Name + "' class='form-control roles-list' disabled='disabled' multiple='multiple'></select></div>"
    var select31Div = "<div class='divTableCell'><select name='" + select3Name + "' class='form-control roles-list' disabled='disabled' multiple='multiple'></select></div>"
    var select41Div = "<div class='divTableCell'><select name='" + select4Name + "' class='form-control roles-list' disabled='disabled' multiple='multiple'></select></div></div>"
    $('#'+id).find('.divTableBody').append(newDiv + select1Div + select21Div + select31Div + select41Div);
    $('.roles-list').first().find('option').each(function () {
        $('#'+ randomId).find('.roles-list').append("<option value='" + $(this).text().trim() + "'>" + $(this).text().trim() + "</option>");
    });
    $('#' + id).next().find('.new-record').val('');

}


function addCustomOwnership(event) {

    event.preventDefault();
    var customRecord = $('#Ownership').next().find('.new-record').val();
    if(!customRecord.trim()) return;
    var checkboxName = encodeInputName('6', 'Ownership of the Model', 'Stage','Customized', customRecord , 'is checked');
    var select1Name = encodeInputName('6', 'Ownership of the Model', 'Stage','Customized', customRecord , 'Owned by') + '[]';
    var select2Name = encodeInputName('6', 'Ownership of the Model', 'Stage','Customized', customRecord , 'Lisenced to') + '[]';
    var randomId = Math.random().toString(36).substring(7);
    var newDiv = "<div id='" + randomId + "'class='divTableRow'><div class='divTableCell'><label><input data-unchecked-value='false' value='true' onchange='enableCurrentRow(this)' name='" + checkboxName + "'type='checkbox'>" + customRecord + "</label></div>";
    var select1Div = "<div class='divTableCell'><select name='" + select1Name + "' class='form-control roles-list' disabled='disabled' multiple='multiple'></select></div>"
    var select21Div = "<div class='divTableCell'><select name='" + select2Name + "' class='form-control roles-list' disabled='disabled' multiple='multiple'></select></div></div>"
    $('#Ownership').find('.divTableBody').append(newDiv + select1Div + select21Div);
    $('.roles-list').first().find('option').each(function () {
        $('#'+ randomId).find('.roles-list').append("<option value='" + $(this).text().trim() + "'>" + $(this).text().trim() + "</option>");
    });

    $('#Ownership').next().find('.new-record').val('');

}


function addCustomSoftwarePlatform(event) {

    event.preventDefault();
    var customRecord = $('#SoftwarePlatform').next().find('.new-record').val();
    if (!customRecord.trim()) return;
    var input1 = encodeInputName('8', 'Software Platform', 'Use', 'Customized', customRecord, 'Software');
    var input2 = encodeInputName('8', 'Software Platform', 'Use', 'Customized', customRecord, 'Version');
    var newDiv = "<div class='divTableRow'><div class='divTableCell'>" + customRecord + "</div><div class='divTableCell'><input name='" + input1 + "'type='text'></div><div class='divTableCell'><input name='" + input2 + "' type='text'></div></div>";
    $('#SoftwarePlatform').find(".divTableBody").append(newDiv);
    $('#SoftwarePlatform').next().find('.new-record').val('');

}


function addCustomCoordinate(event) {

    event.preventDefault();
    var customRecord = $('#Coordinates').next().find('.new-record').val();
    if (!customRecord.trim()) return;
    var count = $('#Coordinates').find('.divTableRow').length;
    customRecord = count + '- ' + customRecord;
    var input1 = encodeInputName('9', 'Coordinates', 'Customized', customRecord);
    var newDiv = "<div class='divTableRow'><div class='divTableCell'>" + customRecord + "</div><div class='divTableCell'><input name='" + input1 + "'type='text'></div></div>";
    $('#Coordinates').find(".divTableBody").append(newDiv);
    $('#Coordinates').next().find('.new-record').val('');
    
}

function addCustomCommunication(e) {
    e.preventDefault();
    var customRecord = $('#Communication').next().find('.new-record').val();
    if (!customRecord.trim()) return;
    var input1 = encodeInputName('10', 'Communication: Coordination and Clash Detection', 'Customized', customRecord);
    var newDiv = "<div class='divTableRow'><div class='divTableCell'>" + customRecord + "</div><div class='divTableCell'><input name='" + input1 + "'type='text'></div></div>";
    $('#Communication').find(".divTableBody").append(newDiv);
    $('#Communication').next().find('.new-record').val('');
}

function addCustomStrategy(e) {

    e.preventDefault();
    var customRecord = $('#AssetStrategy').next().find('.new-record').val();
    if (!customRecord.trim()) return;
    var input1 = encodeInputName('11', 'Asset Information Model Delivery Strategy', 'Customized', customRecord);
    var newDiv = "<div class='divTableRow'><div class='divTableCell'>" + customRecord + "</div><div class='divTableCell'><input name='" + input1 + "'type='text'></div></div>";
    $('#AssetStrategy').find(".divTableBody").append(newDiv);
    $('#AssetStrategy').next().find('.new-record').val('');
}

